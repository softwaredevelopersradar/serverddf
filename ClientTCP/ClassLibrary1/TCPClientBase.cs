﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace ClientTCP
{
    public class TCPClientBase
    {
        public event EventHandler OnConnect;
        public event EventHandler OnDisconnect;
        public event EventHandler OnDestroyServer;

        #region Public Properties
        private volatile bool _ExitSignal;
        public virtual bool ExitSignal
        {
            get => this._ExitSignal;
            set => this._ExitSignal = value;
        }
        #endregion

        #region Variables
        protected readonly string IpAddress = "127.0.0.1";
        protected readonly int Port = 55555;
        protected bool IsRunning;

        protected NetworkStream networkStream;
        protected TcpClient tcpClient;
        #endregion


        #region Constructor
        public TCPClientBase()
        {

        }

        public TCPClientBase(string ipAddress, int port)
        {
            this.Port = port;
            this.IpAddress = ipAddress;
        }

        public TCPClientBase(int port)
        {
            this.Port = port;

            string hostName = Dns.GetHostName();
            IPHostEntry ipHostInfo = Dns.GetHostEntry(hostName);
            this.IpAddress = null;

            for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
            {
                if (ipHostInfo.AddressList[i].AddressFamily ==
                  AddressFamily.InterNetwork)
                {
                    this.IpAddress = ipHostInfo.AddressList[i].ToString();
                    break;
                }
            }
            if (this.IpAddress == null)
                throw new Exception("No IPv4 address for server");

        }

        #endregion


        #region Public Functions
        public virtual void Connect()
        {
            if (this.IsRunning)
                return; //Already running, only one running instance allowed.

            this.IsRunning = true;
            this.ExitSignal = false;

            //while (!this.ExitSignal)
            this.ConnectionLooper();

            this.IsRunning = false;
        }

        public virtual void Disconnect()
        {
            if (this.IsRunning)
                return; //Already running, only one running instance allowed.

            tcpClient.Close();
            this.IsRunning = false;
        }
        #endregion

        #region Protected Functions
        protected async virtual void ConnectionLooper()
        {

            tcpClient = new TcpClient();

            try
            {
                tcpClient.Connect(this.IpAddress, this.Port);
                Console.WriteLine("Attemping server connection... on Thread " + Thread.CurrentThread.ManagedThreadId.ToString());

                OnConnect?.Invoke(this, new EventArgs());
                networkStream = tcpClient.GetStream();



                byte[] b = new byte[5];
                b[0] = 3;
                b[1] = 5;
                await ProcessSend(b);
                await ProcessRead();
            }
            catch
            {

                return;
            }



        }
        #endregion


        private async Task ProcessRead()
        {
            try
            {

                while (true)
                {
                    byte[] buffer = new byte[1024];
                    var count = await networkStream.ReadAsync(buffer, 0, buffer.Length);

                    if (count == 0)
                        break;

                    Array.Resize(ref buffer, count);

                    //OnMessage?.Invoke(this, new ByteEventArgs(buffer));

                    ReceiveMessage(buffer);

                }

                if (tcpClient.Connected)
                {
                    //OnDisconnect?.Invoke(this, new EventArgs());
                    tcpClient.Close();
                }
            }
            catch
            {
                return;
            }

            Console.WriteLine("Can not connect to server " + Thread.CurrentThread.ManagedThreadId.ToString());

            if (tcpClient.Connected)
                tcpClient.Close();
            OnDestroyServer?.Invoke(this, new EventArgs());
        }


        protected async Task<bool> ProcessSend(byte[] b)
        {
            try
            {
                await networkStream.WriteAsync(b, 0, b.Length);
                return true;
            }
            catch 
            {
                return false;
            }
        }

        protected virtual void ReceiveMessage (byte[] b)
        {
            
        }
    }
}

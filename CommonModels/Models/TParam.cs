﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModels.Models
{
    public class TParam
    {
        public byte Gain1 { get; set; }

        public byte Gain2 { get; set; }

        public byte Attenuator1 { get; set; }

        public byte Attenuator2 { get; set; }
    }
}

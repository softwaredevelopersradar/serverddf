﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModels.Models
{
    public class TSpectrum
    {
        public TSpectrum()
        {
            
        }

        public TSpectrum(short IQCount)
        {
            Amplitude = new float[IQCount];
        }
        public DateTime LastTime { get; set; }
        public float[] Amplitude { get; set; }
    }
}

﻿public enum ECodeDDF : byte
{
    MODE = 1,
    STATUS = 2,
    STATUS_RAM = 3,
    PARAM = 4,
    SPECTRUM = 5,
    FRAME = 6
}
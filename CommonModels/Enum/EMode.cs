﻿public enum EMode : byte
{
    Stop = 1,
    Radiointellegence = 2,
    Jamming = 3
}
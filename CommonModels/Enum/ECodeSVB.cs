﻿public enum ECodeSVB : byte
{
    _80 = 1,
    _160 = 2,
    _320 = 3,
    _500 = 4
}
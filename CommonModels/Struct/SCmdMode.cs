﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Struct
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct SCmdMode
    {
        public SCmdMode(EMode mode)
        {
            Mode = mode;
            Data = new byte[1];

            try
            {
                Data[0] = (byte)Mode;
            }
            catch
            { }
        }

        public SCmdMode(byte[] data)
        {            
            Data = data;
            Mode = EMode.Stop;

            try
            {
                Mode = (EMode)Data[0];
            }
            catch { }
                
        }
        public EMode Mode;
        public byte[] Data { get; private set; }
    }
}

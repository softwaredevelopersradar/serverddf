﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Struct
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct SCmdPatternObject
    {
        private const int LengthService = 9;
        public SCmdServicePart ServicePart;
        public Object InformPart;

        public byte[] GetBytesFromStruct()
        {
            try
            {
                //int LengthService = Marshal.SizeOf(ServicePart);

                ServicePart.LenghtInform = InformPart == null ? 0 : Marshal.SizeOf(InformPart);

                byte[] byteSend = new byte[LengthService];
                Array.Copy(Serrialization.StructToByteArray(ServicePart), 0, byteSend, 0, LengthService);


                if (InformPart != null)
                {
                    Array.Resize(ref byteSend, LengthService + ServicePart.LenghtInform);
                    Array.Copy(Serrialization.StructToByteArray(InformPart), 0, byteSend, LengthService, ServicePart.LenghtInform);
                }

                return byteSend;
            }
            catch
            {
                return null;
            }
        }

        public void  GetInformFromBytes(byte[] b)
        {
            object obj = new object();

            try
            {
                Serrialization.ByteArrayToStructure(b, ref obj);

            }
            catch (Exception e)
            {

            }

            this.InformPart = obj;
        }
    }

    
}

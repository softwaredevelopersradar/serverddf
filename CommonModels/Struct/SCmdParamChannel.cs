﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace CommonModels.Struct
{
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Unicode)]
    public struct SCmdParamChannel
    {

        public SCmdParamChannel(byte channel, double frequency, byte gain1, byte gain2, byte attenuator1, byte attenuator2)
        {
            this.Channel = channel;
            this.Frequency = frequency;
            this.Gain1 = gain1;
            this.Gain2 = gain2;
            this.Attenuator1 = attenuator1;
            this.Attenuator2 = attenuator2;
            Data = new byte[13];

            try 
            {
                Data[0] = Channel;
                Array.Copy(Data, 1, BitConverter.GetBytes(Frequency), 0, 8);
                Data[9] = Gain1;
                Data[10] = Gain2;
                Data[11] = Attenuator1;
                Data[12] = Attenuator2;
            }
            catch { }
        }

        public SCmdParamChannel(byte[] data)
        {
            this.Channel = 0;
            this.Frequency = 0;
            this.Gain1 = 0;
            this.Gain2 = 0;
            this.Attenuator1 = 0;
            this.Attenuator2 = 0;
            this.Data = data;
            try
            {

                Channel = Data[0] ;
                Frequency = BitConverter.ToDouble(Data, 1);
                Gain1 = Data[9];
                Gain2 = Data[10];
                Attenuator1 = Data[11];
                Attenuator2 = Data[12];
            }
            catch { }
        }

        public byte Channel { get; set; }

        public double Frequency { get; set; }

        public byte Gain1 { get; set; }

        public byte Gain2 { get; set; }

        public byte Attenuator1 { get; set; }

        public byte Attenuator2 { get; set; }

        public byte[] Data;
    }
}

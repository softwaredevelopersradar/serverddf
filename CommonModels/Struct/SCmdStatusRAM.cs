﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModels.Struct
{
    public struct SCmdStatusRAM
    {
        public SCmdStatusRAM(short version, byte codeFFT, ECodeSVB codeSVB)
        {
            Version = version;

            CodeFFT = codeFFT;

            CodeSVB = codeSVB;

            Data = new byte[4];

            try
            {
                Array.Copy(BitConverter.GetBytes(Version),0,Data,0,2);
                Data[2] = CodeFFT;
                Data[3] = (byte)CodeSVB;
            }
            catch { }
        }

        public SCmdStatusRAM(byte[] data)
        {
            Version = 0;

            CodeFFT = 0;

            CodeSVB = 0;

            Data = data;

            try
            {
                Version = BitConverter.ToInt16(Data, 0);
                CodeFFT = Data[2];
                CodeSVB = (ECodeSVB)Data[3];

            }
            catch { }
        }

        public short Version { get; set; }

        public byte CodeFFT { get; set; }

        public ECodeSVB CodeSVB { get; set; }

        public byte[] Data { get; set; }

        
    }
}

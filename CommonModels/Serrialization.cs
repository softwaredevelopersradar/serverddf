﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonModels
{
    public static class Serrialization
    {
        public static byte[] StructToByteArray(object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure.GetType());

            //sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            byte[] outArray = new byte[sizeInBytes];

            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);
            System.Runtime.InteropServices.Marshal.StructureToPtr(structure, ptr, false);
            System.Runtime.InteropServices.Marshal.Copy(ptr, outArray, 0, sizeInBytes);
            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

            return outArray;
        }

        // function of serialization of bytes array in structure
        public static void ByteArrayToStructure(byte[] bytes, ref object structure)
        {
            int sizeInBytes = System.Runtime.InteropServices.Marshal.SizeOf(structure);
            IntPtr ptr = System.Runtime.InteropServices.Marshal.AllocHGlobal(sizeInBytes);

            System.Runtime.InteropServices.Marshal.Copy(bytes, 0, ptr, sizeInBytes);
            structure = System.Runtime.InteropServices.Marshal.PtrToStructure(ptr, structure.GetType());

            System.Runtime.InteropServices.Marshal.FreeHGlobal(ptr);

        }
    }
}

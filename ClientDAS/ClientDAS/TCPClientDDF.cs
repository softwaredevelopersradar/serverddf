﻿using ClientDDF.ModelEventArgs;
using ClientTCP;
using CommonModels;
using CommonModels.Models;
using CommonModels.Struct;
using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace ClientDDF
{
    public class TCPClientDDF:TCPClientBase
    {
        private const int LEN_HEAD = 9;

        private byte[] buffer = new byte[10000];
        private int lenght;
        private bool exist;

        public TCPClientDDF(string ipAddress, int port)
           : base(ipAddress, port)
        { }

        public event EventHandler<SpectrumEventArgs> OnGetSpectrum;
        public event EventHandler<ParamEventArgs> OnGetParam;
        public event EventHandler <ModeEventArgs> OnGetMode;


        

        #region SendCommand
        public async Task<bool> SetMode(SCmdMode cmdMode)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = cmdMode;

            patternObject.ServicePart.Code = ECodeDDF.MODE;
            patternObject.ServicePart.Error = 0;

            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await ProcessSend(patternObject.GetBytesFromStruct());
        }

        public async Task<bool> SetParam(SCmdParamChannel freqParamChannel)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = new SCmdParamChannel();
            patternObject.InformPart = freqParamChannel;

            patternObject.ServicePart.Code = ECodeDDF.PARAM;
            patternObject.ServicePart.Error = 0;

            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await ProcessSend(patternObject.GetBytesFromStruct());
        }

        public async Task<bool> SetFrame()
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            

            patternObject.ServicePart.Code = ECodeDDF.FRAME;
            patternObject.ServicePart.Error = 0;

            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await ProcessSend(patternObject.GetBytesFromStruct());
        }

        public async Task<bool> GetStatus()
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = null;

            patternObject.ServicePart.Code = ECodeDDF.STATUS;
            patternObject.ServicePart.Error = 0;
            
            return await ProcessSend(patternObject.GetBytesFromStruct());
        }

        public async Task<bool> GetStatusRAM()
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = null;

            patternObject.ServicePart.Code = ECodeDDF.STATUS_RAM;
            patternObject.ServicePart.Error = 0;

            return await ProcessSend(patternObject.GetBytesFromStruct());
        }
        #endregion

        #region ReceiveCommand

        protected async override void ReceiveMessage(byte[] data)
        {
            Console.WriteLine("Receive byte " + data.Length.ToString());// +  System.Text.Encoding.UTF8.GetString(e, 0, e.Length));


            Array.Resize(ref buffer, lenght + data.Length);
            Array.Copy(data, 0, buffer, lenght, data.Length);

            lenght += data.Length;

            exist = true;

            while (lenght >= LEN_HEAD && exist)
            {

                byte[] head = new byte[LEN_HEAD];
                Array.Copy(buffer, 0, head, 0, LEN_HEAD);

                SCmdPatternObject cmdPatternObject = new SCmdPatternObject();
                cmdPatternObject.ServicePart = new SCmdServicePart(head);
               
                int iLengthCmd = cmdPatternObject.ServicePart.LenghtInform;
                if (lenght - LEN_HEAD >= iLengthCmd)
                {
                    byte[] decode = new byte[iLengthCmd];
                    Array.Copy(buffer, LEN_HEAD, decode, 0, iLengthCmd);
                    
                   
                    await DecodeCommand(cmdPatternObject, decode);

                    Array.Reverse(buffer);
                    Array.Resize(ref buffer, buffer.Length - (LEN_HEAD + iLengthCmd));
                    Array.Reverse(buffer);

                    lenght -= (LEN_HEAD + iLengthCmd);

                    exist = true;
                }

                else

                    exist = false;

            } // while (iTempLength > LEN_HEAD)

        }


        private async Task DecodeCommand(SCmdPatternObject CmdPatternObject, byte[] decode)
        {
            
            switch (CmdPatternObject.ServicePart.Code)
            {
                case ECodeDDF.MODE:
                    SCmdMode sCmdMode = new SCmdMode(decode);
                                       
                    break;

                case ECodeDDF.PARAM:                    
                    SCmdParamChannel sCmdParamChannel = new SCmdParamChannel(decode);
                    
                    break;

                case ECodeDDF.SPECTRUM:                    
                    SCmdSingleSpectrum sCmdSingleSpectrum = new SCmdSingleSpectrum();
                    object obj3 = sCmdSingleSpectrum;
                    Serrialization.ByteArrayToStructure(decode, ref obj3);
                    sCmdSingleSpectrum = (SCmdSingleSpectrum)obj3;
                    break;

                case ECodeDDF.STATUS_RAM:
                    
                    SCmdStatusRAM sCmdStatusRAM = new SCmdStatusRAM();
                    object obj4 = sCmdStatusRAM;
                    Serrialization.ByteArrayToStructure(decode, ref obj4);
                    sCmdStatusRAM = (SCmdStatusRAM)obj4;

                    break;

                case ECodeDDF.STATUS:
                
                    break;

                default:
                    break;



            }
        }

        #endregion

    }
}

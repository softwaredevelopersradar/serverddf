﻿using CommonModels;
using CommonModels.Models;
using CommonModels.Struct;
using ServerDDF;
using ServerTCP;
using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ServerDDF
{
    public class TCPServerDDF: TCPServerBase
    {

        public TCPServerDDF(string ipAddress, int port)
        : base(ipAddress, port)
        { }

        protected async override void BaseClient_OnMessage(object sender, ServerTCP.Event.ByteEventArgs e)
        {
            Console.WriteLine("Receive byte " + ((ClientBase)sender).tcpClient.Client.RemoteEndPoint.ToString() + " " + System.Text.Encoding.UTF8.GetString(e.Data, 0, e.Data.Length));
            Console.WriteLine();
        }


        

        public async Task<bool> SendMode(TcpClient tCPClient, SCmdMode cmdMode)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = cmdMode;

            patternObject.ServicePart.Code = ECodeDDF.MODE;
            patternObject.ServicePart.Error = 0;
            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);
           
            return await SendDataClient(tCPClient, patternObject.GetBytesFromStruct());
        }

        public async Task<bool> SendSpectrum(TcpClient tCPClient, SCmdSingleSpectrum singleSpectrum)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = new SCmdSingleSpectrum();
            patternObject.InformPart = singleSpectrum;

            patternObject.ServicePart.Code = ECodeDDF.SPECTRUM;
            patternObject.ServicePart.Error = 0;
            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await SendDataClient(tCPClient, patternObject.GetBytesFromStruct());
        }

        public async Task<bool> SendParam(TcpClient tCPClient, SCmdParamChannel freqParamChannel)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = new SCmdParamChannel();
            patternObject.InformPart = freqParamChannel;

            patternObject.ServicePart.Code = ECodeDDF.PARAM;
            patternObject.ServicePart.Error = 0;

            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await SendDataClient(tCPClient, patternObject.GetBytesFromStruct());
        }

        public async Task<bool> SendConfig(TcpClient tCPClient, SCmdStatusRAM statusRAM)
        {
            SCmdPatternObject patternObject = new SCmdPatternObject();

            patternObject.ServicePart = new SCmdServicePart();
            patternObject.InformPart = new SCmdStatusRAM();
            patternObject.InformPart = statusRAM;

            patternObject.ServicePart.Code = ECodeDDF.STATUS_RAM;
            patternObject.ServicePart.Error = 0;

            patternObject.ServicePart.LenghtInform = Marshal.SizeOf(patternObject.InformPart);

            return await SendDataClient(tCPClient, patternObject.GetBytesFromStruct());
        }



        private async Task<bool> SendDataClient(TcpClient tCPClient, byte[] data)
        {
            Thread.Sleep(50);
            return await ListClients.Find(x => x.tcpClient == tCPClient).DataSend(data);
        }

    }
}

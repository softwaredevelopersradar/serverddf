﻿
using ClientDDF;
using ClientTCP;
using System;

namespace HostClientDDF
{
    class HostConsoleClient
    {
        static void Main(string[] args)
        {
            try
            {
                int port = 6340;
                string ipAddress = "127.0.0.1";

                TCPClientDDF client = new TCPClientDDF(ipAddress, port);

                client.Connect();


                
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}

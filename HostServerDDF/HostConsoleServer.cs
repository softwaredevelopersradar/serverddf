﻿using ClientDataBase;
using CommonModels.Models;
using CommonModels.Struct;
using ServerDDF;
using ServerTCP;
using StorageDDF;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace HostServerDDF
{
    partial class HoctConsole
    {
        static DynamicStorage dynamicStorage = new DynamicStorage();
        //static ObjectsVariable objectsVariable = new ObjectsVariable();

        //internal static TCPServerDDF ServerDDF  = new TCPServerDDF("127.0.0.1", 6340);
        internal static ObjectsVariable objectsVariable = new ObjectsVariable();

        static void Main(string[] args)
        {
            try
            {

                

                objectsVariable.Init();

                ConnectClientDB();

                ConnectServerDDF();

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }


        private static void MessageConsole(string message)
        {
            Console.WriteLine(message);
        }
        

       

        
    }
}

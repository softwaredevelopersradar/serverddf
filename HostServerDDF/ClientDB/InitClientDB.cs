﻿using ClientDataBase;
using GrozaSModelsDBLib;
using System;
using System.Windows;

namespace HostServerDDF
{
    partial class HoctConsole
    {
        

        private static void ConnectClientDB()
        {
            if (objectsVariable.ClientDB != null)
                    DisconnectClientDB();
                try
                {

                    //endPoint = mainWindowViewModel.LocalPropertiesVM.DB.IpAddress.ToString() + ":" + mainWindowViewModel.LocalPropertiesVM.DB.Port;
                    objectsVariable.ClientDB = new ClientDB(objectsVariable.Name, objectsVariable.endPoint);


                    objectsVariable.ClientDB.OnConnect += HandlerConnect_ClientDb;
                    objectsVariable.ClientDB.OnDisconnect += HandlerDisconnect_ClientDb;
                    objectsVariable.ClientDB.OnErrorDataBase += HandlerError_ClientDb;
                    //        (ObjectsVariable.objectClientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable += HandlerUpdate_TableJammer;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable += HandlerUpdate_TableFreqRangesRecon;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable += HandlerUpdate_TableFreqKnown;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable += HandlerUpdate_TableFreqForbidden;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable += HandlerUpdate_TableSectorsRecon;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable += HandlerUpdate_TableSource;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable += HandlerUpdate_TableSuppressSource;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable += HandlerUpdate_TableSuppressGnss;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable += HandlerUpdate_GlobalProperties;
                    //(ObjectsVariable.objectClientDB.Tables[NameTable.TablePattern] as ITableUpdate<TablePattern>).OnUpTable += HandlerUpdate_TablePattern;

                    objectsVariable.ClientDB.ConnectAsync();
                }
                catch (Exception e)
            { }


        }

        private static void DisconnectClientDB()
        {
            //if (ObjectsVariable.objectClientDB != null)
            //{
            //    ObjectsVariable.objectClientDB.Disconnect();

            //    ObjectsVariable.objectClientDB.OnConnect -= HandlerConnect_ClientDb;
            //    ObjectsVariable.objectClientDB.OnDisconnect -= HandlerDisconnect_ClientDb;
            //    ObjectsVariable.objectClientDB.OnErrorDataBase -= HandlerError_ClientDb;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableJammerStation] as ITableUpdate<TableJammerStation>).OnUpTable -= HandlerUpdate_TableJammer;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqRangesRecon] as ITableUpdate<TableFreqRangesRecon>).OnUpTable -= HandlerUpdate_TableFreqRangesRecon;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqKnown] as ITableUpdate<TableFreqKnown>).OnUpTable -= HandlerUpdate_TableFreqKnown;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableFreqForbidden] as ITableUpdate<TableFreqForbidden>).OnUpTable -= HandlerUpdate_TableFreqForbidden;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableSectorsRecon] as ITableUpdate<TableSectorsRecon>).OnUpTable -= HandlerUpdate_TableSectorsRecon;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableSource] as ITableUpdate<TableSource>).OnUpTable -= HandlerUpdate_TableSource;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableSuppressSource] as ITableUpdate<TableSuppressSource>).OnUpTable -= HandlerUpdate_TableSuppressSource;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.TableSuppressGnss] as ITableUpdate<TableSuppressGnss>).OnUpTable -= HandlerUpdate_TableSuppressGnss;
            //    (ObjectsVariable.objectClientDB.Tables[NameTable.GlobalProperties] as ITableUpdate<GlobalProperties>).OnUpTable -= HandlerUpdate_GlobalProperties;

            //    ObjectsVariable.objectClientDB = null;
            //}

        }
    }
}
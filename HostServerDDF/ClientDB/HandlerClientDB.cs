﻿using GrozaSModelsDBLib;
using InheritorsEventArgs;

namespace HostServerDDF
{
    partial class HoctConsole
    {
        private static void HandlerDisconnect_ClientDb(object sender, ClientEventArgs e)
        {
            int g = 0;
        }

        private static void HandlerConnect_ClientDb(object sender, ClientEventArgs e)
        {
            LoadTables();

        }
        private static void HandlerError_ClientDb(object sender, InheritorsEventArgs.OperationTableEventArgs e)
        {

        }

        private static void HandlerUpdate_TableJammer(object sender, InheritorsEventArgs.TableEventArgs<TableJammerStation> e)
        {
        }

        private static void HandlerUpdate_TableFreqRangesRecon(object sender, InheritorsEventArgs.TableEventArgs<TableFreqRangesRecon> e)
        {

        }
        private static void HandlerUpdate_TableFreqKnown(object sender, InheritorsEventArgs.TableEventArgs<TableFreqKnown> e)
        { }

        private static void HandlerUpdate_TableFreqForbidden(object sender, InheritorsEventArgs.TableEventArgs<TableFreqForbidden> e)
        {
        }

        private static void HandlerUpdate_TableSectorsRecon(object sender, TableEventArgs<TableSectorsRecon> e)
        {
        }

        private static void HandlerUpdate_TableSource(object sender, InheritorsEventArgs.TableEventArgs<TableSource> e)
        {
        }

        private static void HandlerUpdate_TableSuppressSource(object sender, TableEventArgs<TableSuppressSource> e)
        {

        }


        private static void HandlerUpdate_TableSuppressGnss(object sender, TableEventArgs<TableSuppressGnss> e)
        {

        }


        private static void HandlerUpdate_GlobalProperties(object sender, InheritorsEventArgs.TableEventArgs<GlobalProperties> e)
        {

        }

        private static void HandlerUpdate_TablePattern(object sender, InheritorsEventArgs.TableEventArgs<TablePattern> e)
        {

        }


        private async static void LoadTables()
        {
            try
            {
                await objectsVariable.ClientDB.Tables[NameTable.TableJammerStation].LoadAsync<TableJammerStation>();
                await objectsVariable.ClientDB.Tables[NameTable.TablePattern].LoadAsync<TablePattern>();

            }
            catch { }
        }
    }
}
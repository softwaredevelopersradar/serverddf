﻿namespace HostServerDDF
{
    partial class HoctConsole
    {
        private static void ConnectServerDDF()
        {
            objectsVariable.ServerDDF.OnConnectClient += Service_OnConnectClient;

            objectsVariable.ServerDDF.Create();
        }
    }
}
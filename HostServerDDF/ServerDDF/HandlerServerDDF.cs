﻿using CommonModels.Struct;
using System.Net.Sockets;

namespace HostServerDDF
{
    partial class HoctConsole
    {
        private async static void Service_OnConnectClient(object sender, TcpClient e)
        {

            //MessageConsole(string message);


            await objectsVariable.ServerDDF.SendMode(e, new SCmdMode() { Mode = dynamicStorage.CurrentMode });




            foreach (var ch in dynamicStorage.Channel)
            {
                SCmdParamChannel freqParamChannel = new SCmdParamChannel()
                {
                    Channel = (byte)ch.Type,
                    Frequency = ch.Frequency,
                    Gain1 = ch.Param.Gain1,
                    Gain2 = ch.Param.Gain2,
                    Attenuator1 = ch.Param.Attenuator1,
                    Attenuator2 = ch.Param.Attenuator2
                };

                await objectsVariable.ServerDDF.SendParam(e, freqParamChannel);
            }




        }
    }
}
﻿using ServerTCP.Interface;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ServerTCP
{
    
    public class TCPServerBase
    {

        #region Public Properties
        private volatile bool _ExitSignal;
        public virtual bool ExitSignal
        {
            get => this._ExitSignal;
            set => this._ExitSignal = value;
        }
        #endregion


        public event EventHandler<TcpClient> OnConnectClient;
        public event EventHandler<TcpClient> OnDisconnectClient;
        public event EventHandler OnDestroyServer;

        


        #region Variables
        protected readonly byte MaxClients  = 10;
        protected readonly string IpAddress = "127.0.0.1";
        protected readonly int Port = 55555;
        protected List<ClientBase> ListClients = new List<ClientBase>();

        protected bool IsRunning = false;

        protected readonly TcpListener listener;
        #endregion


        #region Constructor
        public TCPServerBase()
        {            
            listener = new TcpListener(IPAddress.Parse(this.IpAddress), this.Port);
        }

        public TCPServerBase(string ipAddress, int port)
        {
            this.Port = port;
            this.IpAddress = ipAddress;

            if (this.IpAddress == null)
                throw new Exception("No IP address for server");

            listener = new TcpListener(IPAddress.Parse(this.IpAddress), this.Port);
        }

        public TCPServerBase(int port) 
        {
            this.Port = port;

            string hostName = Dns.GetHostName();
            IPHostEntry ipHostInfo = Dns.GetHostEntry(hostName);
            this.IpAddress = null;

            for (int i = 0; i < ipHostInfo.AddressList.Length; ++i)
            {
                if (ipHostInfo.AddressList[i].AddressFamily ==
                  AddressFamily.InterNetwork)
                {
                    this.IpAddress = ipHostInfo.AddressList[i].ToString();
                    break;
                }
            }
            if (this.IpAddress == null)
                throw new Exception("No IPv4 address for server");

            listener = new TcpListener(IPAddress.Parse(this.IpAddress), this.Port);
        }

        #endregion


        #region Public function
        
        public async void Create()
        {
            if (this.IsRunning)
                return; //Already running, only one running instance allowed.

            this.IsRunning = true;
            this.listener.Start();
            Console.WriteLine("Create server " + this.IpAddress + " :" + this.Port.ToString());
            this.ExitSignal = false;

            while (!this.ExitSignal)
                await ConnectionLooper();

            this.IsRunning = false;

        }

        public async void Destroy()
        {
            listener.Stop();

            OnDestroyServer?.Invoke(this, new EventArgs());

            Console.WriteLine("Destroy server " + this.IpAddress.ToString() + " :" + this.Port.ToString());
        }


        #endregion


        #region Protected function
        protected async Task ConnectionLooper()
        {
            while (this.IsRunning && ListClients.Count < MaxClients)
            {
                try
                {
                    TcpClient tcpClient = await listener.AcceptTcpClientAsync();

                    Task t = ProcessMessagesFromClient(tcpClient);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }


        protected async Task ProcessMessagesFromClient(TcpClient tcpClient)
        {
            
                Console.WriteLine("Connect client " + tcpClient.Client.RemoteEndPoint.ToString() + Thread.CurrentThread.ManagedThreadId.ToString());
                if (!tcpClient.Connected) //Abort if not connected
                    return;

                ClientAdd(tcpClient);

                OnConnectClient?.Invoke(this, tcpClient);

                await TaskClientRead(ListClients[ListClients.Count-1]);
            
                ClientRemove(tcpClient);

            if (tcpClient.Connected)
                tcpClient.Close();

        }

        private async Task TaskClientRead(ClientBase baseClient)
        {
            
            await baseClient.ProcessRead();

        }


        private void ClientAdd(TcpClient tcpClient)
        {
            ClientBase baseClient = new ClientBase(tcpClient);
            baseClient.OnMessage += BaseClient_OnMessage;
            ListClients.Add(baseClient);
        }

        private void ClientRemove(TcpClient tcpClient)
        {

            Console.WriteLine("Disconnect client " + tcpClient.Client.RemoteEndPoint.ToString());// + Thread.CurrentThread.ManagedThreadId.ToString());

            OnDisconnectClient?.Invoke(this, tcpClient);

            ListClients.RemoveAt(ListClients.IndexOf(ListClients.Find(x => x.tcpClient.Client == tcpClient.Client)));

            
        }
        
        
        protected virtual void BaseClient_OnMessage(object sender, Event.ByteEventArgs e)
        {           
            Console.WriteLine("Receive byte " + ((ClientBase)sender).tcpClient.Client.RemoteEndPoint.ToString()+" "+ System.Text.Encoding.UTF8.GetString(e.Data, 0, e.Data.Length));
        }

        #endregion
    }
}

﻿using ServerTCP.Event;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerTCP
{
    public class ClientBase
    {
        NetworkStream networkStream;       
        public TcpClient tcpClient;


        public event EventHandler <ByteEventArgs> OnMessage;
        public event EventHandler<EventArgs> OnDisconnect;

        public ClientBase(TcpClient tcpClientInput)
        {
            tcpClient = tcpClientInput;
            networkStream = tcpClient.GetStream();            
        }


        public async Task ProcessRead()
        {
            try
            {                
                while (true)
                {
                    byte[] buffer = new byte[1024];
                    var count = await networkStream.ReadAsync(buffer, 0, buffer.Length);
                    
                    if (count == 0)
                        break;

                    Array.Resize(ref buffer, count);

                    OnMessage?.Invoke(this, new ByteEventArgs(buffer));

                }

                
            }
            catch { }
           
        }

        public async Task<bool> DataSend(byte[] data)
        {
            try
            {
                await networkStream.WriteAsync(data, 0, data.Length);
                return true;

            }
            catch { return false; }
        }


        
    }
}

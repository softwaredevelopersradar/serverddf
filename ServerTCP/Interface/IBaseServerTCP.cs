﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ServerTCP.Interface
{
    interface IBaseServerTCP
    {


        void Create();
        void Destroy();

        event EventHandler OnConnectClient;
        event EventHandler OnDisconnectClient;
        event EventHandler OnDestroyServer;

    }
}

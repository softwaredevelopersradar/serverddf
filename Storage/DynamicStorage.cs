﻿
using CommonModels.Models;
using CommonModels.Struct;
using System;
using System.Collections.Generic;

namespace StorageDDF
{
    public class DynamicStorage
    {


        public DynamicStorage()
        {
            Channel.Add(new StorageDDF.Channel());
            Channel.Add(new StorageDDF.Channel());
        }
        #region Properties
        public EMode CurrentMode { get; set; } = EMode.Stop;

        public List<Channel> Channel { get; set; } = new List<Channel>(2);

        public SCmdStatusRAM StatusRAM { get; set; } = new SCmdStatusRAM();

        #endregion
    }
}

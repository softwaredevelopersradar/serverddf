﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageDDF
{
    public class TSingleViewBand
    {
        public TSingleViewBand()
        { }

        public TSingleViewBand(double FreqCenter, float Band, short IQCount=16634)
        {
            Active = false;
            Range = new TRange(FreqCenter - Band / 2.0, FreqCenter + Band / 2.0);
            Spectrum = new TSpectrum(IQCount);
        }
        public bool Active { get; set; }

        public TRange Range { get; private set;}

        public TSpectrum Spectrum { get; set; }
    }
}

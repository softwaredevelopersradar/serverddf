﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageDDF
{
    public class TRange
    {
        public TRange()
        { }

        public TRange(double freqMinKhz, double freqMaxKhz)
        {
            FreqMinKhz = freqMinKhz;
            FreqMaxKhz = freqMaxKhz;
        }
        public double FreqMinKhz { get; private set; }
        public double FreqMaxKhz { get; private set; }
    }
}

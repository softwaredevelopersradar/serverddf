﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StorageDDF
{
    public class TSpectrum
    {
        public TSpectrum()
        {
            
        }

        public TSpectrum(short IQCount)
        {
            Amplitude = new float[IQCount];
        }
        public DateTime LastTime { get; set; }
        float[] Amplitude { get; set; }
    }
}
